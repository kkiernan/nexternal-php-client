<?php

namespace KKiernan;

use GuzzleHttp\Client;
use SimpleXMLElement;
use Exception;

class Nexternal
{
    /**
     * Fetches orders that have ids in the given range.
     *
     * @param integer $start
     * @param integer $end
     *
     * @return SimpleXMLElement
     */
    public function orderNoRange($start, $end = '')
    {
        return (new OrderQueryRequest())->orderNoRange($start, $end);
    }

    /**
     * Fetches orders in the given date range.
     *
     * @param string $start
     * @param string $end
     *
     * @return SimpleXMLElement
     */
    public function orderDateRange($start, $end = null)
    {
        return (new OrderQueryRequest())->orderDateRange($start, $end);
    }

    /**
     * Fetches customers that have ids in the given range.
     *
     * @param integer $start
     * @param integer $end
     * 
     * @return SimpleXMLElement
     */
    public function customerNoRange($start, $end = '')
    {
        return (new CustomerQueryRequest())->customerNoRange($start, $end);
    }
}
