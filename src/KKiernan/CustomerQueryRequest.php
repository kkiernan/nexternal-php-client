<?php

namespace KKiernan;

class CustomerQueryRequest extends Request
{
    /**
     * @var string
     */
    protected $url = 'https://www.nexternal.com/shared/xml/customerquery.rest';

    /**
     * Creates a new OrderQueryRequest instance.
     */
    public function __construct()
    {
        parent::__construct('CustomerQueryRequest');
    }

    /**
     * Fetches customers that have ids in the given range.
     *
     * @param integer $start
     * @param integer $end
     * 
     * @return SimpleXMLElement
     */
    public function customerNoRange($start, $end = '')
    {
        $this->xml->addChild('CustomerNoRange')
                  ->addChild('CustomerNoStart', $start);

        if (!empty($end)) {
            $this->xml->CustomerNoRange->addChild('CustomerNoEnd', $end);
        }

        return $this->send();
    }
}
