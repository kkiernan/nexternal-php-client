<?php

namespace KKiernan;

use Exception;
use SimpleXMLElement;

class OrderQueryRequest extends Request
{
    /**
     * @var string
     */
    protected $url = 'https://www.nexternal.com/shared/xml/orderquery.rest';

    /**
     * Creates a new OrderQueryRequest instance.
     */
    public function __construct()
    {
        parent::__construct('OrderQueryRequest');
    }

    /**
     * Fetches orders that have ids in the given range.
     *
     * @param integer $start
     * @param integer $end
     *
     * @return SimpleXMLElement
     */
    public function orderNoRange($start, $end = '')
    {
        $this->xml->addChild('OrderNoRange')
                  ->addChild('OrderNoStart', $start);

        if (!empty($end)) {
            $this->xml->OrderNoRange->addChild('OrderNoEnd', $end);
        }

        return $this->send();
    }

    /**
     * Fetches orders in the given date range.
     *
     * @param string $start
     * @param string $end
     *
     * @return SimpleXMLElement
     */
    public function orderDateRange($start, $end = '')
    {
        if (empty($end)) {
            $end = date('Y-m-d H:i:s');
        }

        $this->xml->addChild('OrderDateRange')
                  ->addChild('OrderDateStart')
                  ->addChild('DateTime')
                  ->addChild('Date', date('m/d/Y', strtotime($start)));

        $this->xml->OrderDateRange->OrderDateStart->DateTime->addChild('Time', date('H:i', strtotime($start)));

        $this->xml->OrderDateRange
                  ->addChild('OrderDateEnd')
                  ->addChild('DateTime')
                  ->addChild('Date', date('m/d/Y', strtotime($end)));

        $this->xml->OrderDateRange->OrderDateEnd->DateTime->addChild('Time', date('H:i', strtotime($end)));

        return $this->send();
    }
}
