<?php

namespace KKiernan;

use Exception;
use GuzzleHttp\Client;
use Monolog\Handler\StreamHandler;
use Monolog\Logger;
use SimpleXMLElement;

abstract class Request
{
    /**
     * @var GuzzleHttp\Client
     */
    protected $client;

    /**
     * @var SimpleXMLElement
     */
    protected $xml;

    /**
     * @var Monolog\Logger
     */
    protected $log;

    /**
     * Creates a new Request instance.
     */
    public function __construct($type)
    {
        $this->client = new Client([
            'headers' => [
                'Content-Type' => 'application/x-www-form-urlencoded'
            ],
            'verify' => false,
        ]);

        $this->log = new Logger('debug');
        $this->log->pushHandler(new StreamHandler('debug.log'));

        $this->xml = new SimpleXMLElement("<$type></$type>");
        $this->xml->addChild('Credentials')->addChild('AccountName', getenv('NEXTERNAL_ACCOUNT_NAME'));
        $this->xml->Credentials->addChild('Key', getenv('NEXTERNAL_KEY'));
    }    

    /**
     * Sends a request to the Nexternal XML API.
     *
     * @throws Exception
     * 
     * @return SimpleXMLElement
     */
    public function send()
    {
        $response = $this->client->request('POST', $this->url, ['body' => $this->xml->asXML()]);

        if (getenv('NEXTERNAL_DEBUG') == 'true') {
            $this->log->addInfo($response->getBody()->getContents());
        }

        $response = simplexml_load_string($response->getBody()->getContents());

        if (isset($response->Error)) {
            throw new Exception($response->Error->ErrorDescription);
        }

        return $response;
    }
}
