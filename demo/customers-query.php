<?php

use Dotenv\Dotenv;
use KKiernan\Nexternal;

/*
|---------------------------------------------------------------------
| Require Files
|---------------------------------------------------------------------
*/

require dirname(__DIR__) . '/vendor/autoload.php';

/*
|---------------------------------------------------------------------
| Load Environment Variables
|---------------------------------------------------------------------
*/

$dotenv = new Dotenv(dirname(__DIR__));
$dotenv->load();

/*
|---------------------------------------------------------------------
| Fetch Customers
|---------------------------------------------------------------------
*/

$nexternal = new Nexternal();
$reponse = $nexternal->customerNoRange(1560, 1562);
print_r($reponse);
