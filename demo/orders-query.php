<?php

use Dotenv\Dotenv;
use KKiernan\Nexternal;

/*
|---------------------------------------------------------------------
| Require Files
|---------------------------------------------------------------------
*/

require dirname(__DIR__) . '/vendor/autoload.php';

/*
|---------------------------------------------------------------------
| Load Environment Variables
|---------------------------------------------------------------------
*/

$dotenv = new Dotenv(dirname(__DIR__));
$dotenv->load();

/*
|---------------------------------------------------------------------
| Fetch Orders
|---------------------------------------------------------------------
*/

$nexternal = new Nexternal();

// Fetch orders using an id range
$response = $nexternal->orderNoRange(106108);
print_r($response);

// Fetch orders using a date range
$response = $nexternal->orderDateRange('2016-03-17');
print_r($response);
